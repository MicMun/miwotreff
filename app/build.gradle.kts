plugins {
   alias(libs.plugins.android.application)
   alias(libs.plugins.jetbrains.kotlin.android)
   alias(libs.plugins.devtools.ksp)
   alias(libs.plugins.compose.compiler)
   alias(libs.plugins.kotlin.serialization)
   alias(libs.plugins.dagger.hilt)
}

android {
   namespace = "de.micmun.android.miwotreff"
   compileSdk = 35

   defaultConfig {
      applicationId = "de.micmun.android.miwotreff"
      minSdk = 29
      targetSdk = 35
      versionCode = 700
      versionName = "7.0.0"

      testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
      vectorDrawables {
         useSupportLibrary = true
      }

      ksp {
         arg("room.schemaLocation", "$projectDir/schemas")
      }
   }

   buildTypes {
      release {
         isMinifyEnabled = true
         proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
      }
      debug {
         applicationIdSuffix = ".debug" // erweitere Package
         versionNameSuffix = " [DEBUG]" // erweitere Versions-Name
      }
   }
   compileOptions {
      sourceCompatibility = JavaVersion.VERSION_11
      targetCompatibility = JavaVersion.VERSION_11
   }
   kotlinOptions {
      jvmTarget = "11"
   }
   buildFeatures {
      compose = true
   }
   composeOptions {
      kotlinCompilerExtensionVersion = "1.5.1"
   }
   packaging {
      resources {
         excludes += "/META-INF/{AL2.0,LGPL2.1}"
      }
   }
}

dependencies {

   implementation(libs.androidx.core.ktx)
   implementation(libs.androidx.lifecycle.runtime.ktx)
   implementation(libs.androidx.activity.compose)
   implementation(platform(libs.androidx.compose.bom))
   implementation(libs.androidx.ui)
   implementation(libs.androidx.ui.graphics)
   implementation(libs.androidx.ui.tooling.preview)
   implementation(libs.androidx.material3)
   implementation(libs.androidx.constraintlayout)
   implementation(libs.androidx.livedata)
   implementation(libs.androidx.viewmodel)
   implementation(libs.androidx.room.runtime)
   implementation(libs.androidx.room.ktx)
   implementation(libs.androidx.compose.material3.adaptive.navigation.suite)
   implementation(libs.androidx.compose.material3.adaptive.navigation)
   implementation(libs.kotlinx.serialization.json)

   annotationProcessor(libs.androidx.room.compiler)
   ksp(libs.androidx.room.compiler)

   // Nav Destinations
   implementation(libs.compose.destinations.core)
   ksp(libs.compose.destinations.ksp)

   // Retrofit
   implementation(libs.squareup.retrofit)
   implementation(libs.squareup.converter)
   implementation(libs.squareup.moshi.kotlin)
   implementation(libs.okhttp3)

   // Dagger - Hilt
   implementation(libs.dagger.hilt.android)
   ksp(libs.dagger.hilt.android.compiler)
   ksp(libs.androidx.hilt.compiler)
   implementation(libs.androidx.hilt.navigation)
   implementation(libs.androidx.hilt.work)

   // more icons
   implementation(libs.androidx.compose.icons)

   // Swipe to refresh
   implementation(libs.accompanist.swiperefresh)
   // system ui controller
   implementation(libs.accompanist.systemuicontroller)

   // datastore
   implementation(libs.androidx.datastore)
   implementation(libs.kotlinx.collection.immitable)

   // kotlin + work
   implementation(libs.androidx.work.ktx)

   // permissions
   implementation(libs.accompanist.permissions)

   // storage helper
   implementation(libs.anggrayudi.storage)

   testImplementation(libs.junit)
   androidTestImplementation(libs.androidx.junit)
   androidTestImplementation(libs.androidx.espresso.core)
   androidTestImplementation(platform(libs.androidx.compose.bom))
   androidTestImplementation(libs.androidx.ui.test.junit4)
   debugImplementation(libs.androidx.ui.tooling)
   debugImplementation(libs.androidx.ui.test.manifest)
}