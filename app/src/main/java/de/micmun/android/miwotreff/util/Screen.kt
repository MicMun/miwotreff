/*
 * Screen.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.util

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material.icons.automirrored.outlined.List
import androidx.compose.material.icons.filled.Backup
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.outlined.Backup
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.ui.graphics.vector.ImageVector
import com.ramcosta.composedestinations.generated.destinations.BackupScreenDestination
import com.ramcosta.composedestinations.generated.destinations.ProgramListScreenDestination
import com.ramcosta.composedestinations.generated.destinations.SettingsScreenDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec
import de.micmun.android.miwotreff.R

/**
 * Screen routes.
 *
 * @author MicMun
 * @version 1.0, 08.08.24
 */
enum class Screen(
   val titleId: Int,
   val selectedIcon: ImageVector,
   val unselectedIcon: ImageVector,
   val direction: DirectionDestinationSpec,
   val tooltipId: Int
) {
   PROGRAM_LIST_SCREEN(
      titleId = R.string.screen_title_program,
      selectedIcon = Icons.AutoMirrored.Filled.List,
      unselectedIcon = Icons.AutoMirrored.Outlined.List,
      direction = ProgramListScreenDestination,
      tooltipId = R.string.screen_tooltip_program
   ),
   BACKUP_SCREEN(
      titleId = R.string.screen_title_backup,
      selectedIcon = Icons.Filled.Backup,
      unselectedIcon = Icons.Outlined.Backup,
      direction = BackupScreenDestination,
      tooltipId = R.string.screen_tooltip_backup
   ),
   SETTING_SCREEN(
      titleId = R.string.screen_title_settings,
      selectedIcon = Icons.Filled.Settings,
      unselectedIcon = Icons.Outlined.Settings,
      direction = SettingsScreenDestination,
      tooltipId = R.string.screen_tooltip_settings
   )
}
