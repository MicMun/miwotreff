package de.micmun.android.miwotreff.feature_program.presentation.programs

import de.micmun.android.miwotreff.feature_program.domain.model.FetchProgramResult
import de.micmun.android.miwotreff.feature_program.domain.model.Program

/**
 * State of the screen.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */
data class ProgramListState(
   val programs: List<Program> = emptyList(),
   val isLoading: Boolean = false,
   val isRefreshing: Boolean = false,
   val isSearching: Boolean = false,
   val error: String = "",
   val fetchProgramResult: FetchProgramResult? = null,
   val searchQuery: String = ""
)
