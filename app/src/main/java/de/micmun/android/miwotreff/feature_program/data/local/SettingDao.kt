/*
 * SettingDao.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.micmun.android.miwotreff.feature_program.data.local.entities.SettingEntity

/**
 * Dao for settings.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
@Dao
interface SettingDao {
   @Query("SELECT * FROM settings")
   suspend fun getSettings(): List<SettingEntity>

   @Query("SELECT * FROM settings WHERE `key` = :key")
   suspend fun findSettingByKey(key: String): SettingEntity?

   @Insert(onConflict = OnConflictStrategy.REPLACE)
   suspend fun insertSetting(setting: SettingEntity)
}
