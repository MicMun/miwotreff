/*
 * ProgramApi.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.remote

import de.micmun.android.miwotreff.feature_program.data.remote.dto.LastUpdateDto
import de.micmun.android.miwotreff.feature_program.data.remote.dto.ProgramDto
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * API for program entries (mittwochstreff-muenchen.de).
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
interface ProgramApi {
   @GET("program/api/index.php?op=0")
   suspend fun getProgramEntries(
      @Query("von") dateVon: String
   ): List<ProgramDto>

   @GET("program/api/index.php?op=1")
   suspend fun getLastUpdate(): LastUpdateDto
}
