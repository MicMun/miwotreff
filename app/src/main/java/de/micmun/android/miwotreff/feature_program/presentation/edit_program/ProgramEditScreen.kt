/*
 * ProgramEditScreen.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.edit_program

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.feature_program.presentation.components.AppBarAction
import de.micmun.android.miwotreff.feature_program.presentation.components.ProgramTopBar
import de.micmun.android.miwotreff.util.toUiDate
import kotlinx.coroutines.launch

/**
 * Screen for edit program.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */
@Suppress("UNUSED_PARAMETER")
@OptIn(ExperimentalMaterial3Api::class)
@Destination<RootGraph>
@Composable
fun ProgramEditScreen(
   programId: Int,
   navigator: DestinationsNavigator,
   viewModel: ProgramEditViewModel = hiltViewModel()
) {
   val state = viewModel.state
   val snackbarHostState = remember {
      SnackbarHostState()
   }
   val scope = rememberCoroutineScope()
   var currentProgram = state.program

   Scaffold(topBar = {
      ProgramTopBar(
         title = stringResource(id = R.string.edit_title),
         needsNavigationIcon = true,
         navigationIconClicked = { navigator.navigateUp() },
         actions = listOf(
            AppBarAction(
               title = stringResource(id = R.string.edit_save),
               icon = Icons.Default.Save,
               enabled = true,
               onClick = {
                  currentProgram?.let { p ->
                     viewModel.saveProgram(p)
                     navigator.navigateUp()
                  }
               }
            )
         )
      )
   },
      snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
   ) { innerPaddings ->
      state.program?.let { program ->
         var topic by remember {
            mutableStateOf(program.topic)
         }
         var person by remember {
            mutableStateOf(program.person)
         }
         val topicChanged = { text: String ->
            topic = text
            currentProgram = Program(program.id, program.date, topic, person)
         }
         val personChanged = { text: String ->
            person = text
            currentProgram = Program(program.id, program.date, topic, person)
         }
         var error by remember {
            mutableStateOf(false)
         }

         Column(
            modifier = Modifier
               .padding(innerPaddings)
               .fillMaxSize()
               .padding(horizontal = 8.dp, vertical = 16.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
         ) {
            OutlinedTextField(
               value = program.date.toUiDate(),
               onValueChange = {},
               readOnly = true,
               modifier = Modifier.fillMaxWidth(),
               textStyle = MaterialTheme.typography.bodyLarge
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
               value = topic,
               onValueChange = topicChanged,
               label = {
                  Text(
                     text = stringResource(id = R.string.edit_topic_label),
                     style = MaterialTheme.typography.labelLarge
                  )
               },
               supportingText = {
                  if (topic.isBlank()) {
                     error = true
                     Text(
                        text = stringResource(id = R.string.edit_error_topic_empty),
                        style = MaterialTheme.typography.labelMedium
                     )
                  } else {
                     error = false
                     Text(
                        text = stringResource(id = R.string.edit_required),
                        style = MaterialTheme.typography.labelMedium
                     )
                  }
               },
               isError = error,
               modifier = Modifier.fillMaxWidth(),
               textStyle = MaterialTheme.typography.bodyLarge
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
               value = person ?: "",
               onValueChange = personChanged,
               label = {
                  Text(
                     text = stringResource(id = R.string.edit_person_label),
                     style = MaterialTheme.typography.labelLarge
                  )
               },
               supportingText = {
                  Text(
                     text = stringResource(id = R.string.edit_optional),
                     style = MaterialTheme.typography.labelMedium
                  )
               },
               modifier = Modifier.fillMaxWidth(),
               textStyle = MaterialTheme.typography.bodyLarge
            )
         }
         if (state.error.isNotBlank()) {
            scope.launch {
               snackbarHostState.showSnackbar(
                  message = state.error, duration = SnackbarDuration.Long
               )
            }
         }
      }
   }

}
