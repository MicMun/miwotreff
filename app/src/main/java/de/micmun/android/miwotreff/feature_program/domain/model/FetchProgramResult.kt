package de.micmun.android.miwotreff.feature_program.domain.model

/**
 * Result of fetching program from api.
 *
 * @author MicMun
 * @version 1.0, 08.08.24
 */
data class FetchProgramResult(
   var countNew: Int = -1,
   var countUpdate: Int = -1
)
