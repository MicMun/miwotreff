package de.micmun.android.miwotreff.feature_program.data.remote.dto


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProgramDto(
   @Json(name = "p_date")
   val pDate: String,
   @Json(name = "p_topic")
   val pTopic: String,
   @Json(name = "p_person")
   val pPerson: String
)
