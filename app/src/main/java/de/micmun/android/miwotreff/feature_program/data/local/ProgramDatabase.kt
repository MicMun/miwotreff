/*
 * ProgramDatabase.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.micmun.android.miwotreff.feature_program.data.local.entities.ProgramEntity
import de.micmun.android.miwotreff.feature_program.data.local.entities.SettingEntity

/**
 * Room database for programs.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
@Database(
   entities = [ProgramEntity::class, SettingEntity::class],
   version = 3
)
@TypeConverters(DateConverter::class)
abstract class ProgramDatabase : RoomDatabase() {

   abstract val programDao: ProgramDao
   abstract val settingDao: SettingDao

   companion object {
      const val DATABASE_NAME = "miwotreff"
   }
}
