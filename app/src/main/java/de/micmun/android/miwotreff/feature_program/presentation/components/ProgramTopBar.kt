/*
 * ProgramTopBar.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import de.micmun.android.miwotreff.R

/**
 * TopAppBar for program screens.
 *
 * @author MicMun
 * @version 1.0, 24.08.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProgramTopBar(
   title: String = "",
   needsNavigationIcon: Boolean = false,
   navigationIconClicked: () -> Unit = {},
   actions: List<AppBarAction> = emptyList(),
   scrollBehavior: TopAppBarScrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()
) {
   CenterAlignedTopAppBar(
      title = {
         Text(text = title)
      },
      navigationIcon = {
         if (needsNavigationIcon) {
            CustomIconButton(
               icon = Icons.AutoMirrored.Default.ArrowBack,
               description = stringResource(id = R.string.tip_go_back),
               tooltip = stringResource(id = R.string.tip_go_back),
               onClicked = navigationIconClicked
            )
         }
      },
      actions = {
         actions.forEach { action ->
            CustomIconButton(
               icon = action.icon,
               description = action.title,
               tooltip = action.title,
               enabled = action.enabled,
               onClicked = action.onClick
            )
         }
      },
      scrollBehavior = scrollBehavior,
      colors = TopAppBarDefaults.topAppBarColors(
         containerColor = MaterialTheme.colorScheme.primaryContainer,
         titleContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
         actionIconContentColor = MaterialTheme.colorScheme.onPrimaryContainer,
         navigationIconContentColor = MaterialTheme.colorScheme.primary
      )
   )
}

data class AppBarAction(
   val title: String,
   val icon: ImageVector,
   val enabled: Boolean,
   val onClick: () -> Unit = {}
)
