package de.micmun.android.miwotreff.feature_backup.domain.model

import androidx.compose.runtime.Immutable
import java.time.LocalDateTime

/**
 * Data class for a file.
 *
 * @author MicMun
 * @version 1.0, 14.09.24
 */
@Immutable
data class FileInfo(
   val name: String,
   val path: String,
   val lastModified: LocalDateTime
)
