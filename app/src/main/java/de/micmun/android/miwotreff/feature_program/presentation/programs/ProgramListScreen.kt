/*
 * ProgramListScreen.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.programs

import android.content.Context
import android.content.Intent
import android.icu.util.TimeZone
import android.provider.CalendarContract
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Event
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.adaptive.currentWindowAdaptiveInfo
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.window.core.layout.WindowWidthSizeClass
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootGraph
import com.ramcosta.composedestinations.generated.destinations.ProgramEditScreenDestination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.feature_program.presentation.components.AppBarAction
import de.micmun.android.miwotreff.feature_program.presentation.components.ProgramTopBar
import de.micmun.android.miwotreff.feature_program.presentation.programs.components.ActionIcon
import de.micmun.android.miwotreff.feature_program.presentation.programs.components.ProgramItem
import de.micmun.android.miwotreff.feature_program.presentation.programs.components.SwipeableItemWithActions
import de.micmun.android.miwotreff.util.getOffsetDateTimeInMillis
import kotlinx.coroutines.launch
import java.time.LocalTime

/**
 * Screen for program list.
 *
 * @author MicMun
 * @version 1.0, 08.08.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Destination<RootGraph>(start = true)
@Composable
fun ProgramListScreen(
   navigator: DestinationsNavigator,
   viewModel: ProgramListViewModel = hiltViewModel()
) {
   val state = viewModel.state
   val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = state.isRefreshing)
   val snackbarHostState = remember {
      SnackbarHostState()
   }
   val scope = rememberCoroutineScope()
   val context = LocalContext.current
   val windowWidthClass = currentWindowAdaptiveInfo().windowSizeClass.windowWidthSizeClass
   val compact by remember {
      mutableStateOf(windowWidthClass == WindowWidthSizeClass.COMPACT)
   }

   val scollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()

   Scaffold(
      topBar = {
         ProgramTopBar(
            title = context.getString(R.string.app_name),
            actions = listOf(
               AppBarAction(
                  title = stringResource(id = R.string.search_title),
                  icon = Icons.Default.Search,
                  enabled = !state.isLoading,
                  onClick = {
                     if (!state.isSearching)
                        viewModel.onEvent(ProgramListEvent.ShowSearchField(true))
                     else {
                        viewModel.onEvent(ProgramListEvent.OnSearchQueryChange(""))
                        viewModel.onEvent(ProgramListEvent.ShowSearchField(false))
                     }
                  }
               ),
               AppBarAction(
                  title = stringResource(id = R.string.refresh_title),
                  icon = Icons.Default.Refresh,
                  enabled = !state.isLoading,
                  onClick = {
                     viewModel.onEvent(ProgramListEvent.Refresh)
                  }
               )
            ),
            scrollBehavior = scollBehavior
         )
      },
      snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
      modifier = Modifier
         .fillMaxSize()
         .nestedScroll(scollBehavior.nestedScrollConnection)
   ) { innerPaddings ->
      Box(
         modifier = Modifier
            .fillMaxSize()
            .padding(innerPaddings),
         contentAlignment = Alignment.Center
      ) {
         Column(
            modifier = Modifier
               .fillMaxSize()
               .padding(8.dp)
         ) {
            AnimatedVisibility(
               visible = state.isSearching,
               enter = fadeIn() + slideInVertically(),
               exit = fadeOut() + slideOutVertically()
            ) {
               OutlinedTextField(
                  value = state.searchQuery,
                  onValueChange = {
                     viewModel.onEvent(ProgramListEvent.OnSearchQueryChange(it))
                  },
                  modifier = Modifier
                     .padding(16.dp)
                     .fillMaxWidth(),
                  placeholder = {
                     Text(text = stringResource(id = R.string.search_placeholder))
                  },
                  maxLines = 1,
                  singleLine = true
               )
               Spacer(modifier = Modifier.height(16.dp))
            }

            SwipeRefresh(state = swipeRefreshState,
               onRefresh = {
                  if (!state.isLoading)
                     viewModel.onEvent(ProgramListEvent.Refresh)
               }
            ) {
               val lazyColumnState = rememberLazyListState()

               LaunchedEffect(key1 = state.programs) {
                  if (state.programs.isNotEmpty()) {
                     val current = state.programs.find { it.isCurrentWednesday() }
                     var startIndex = 0
                     current?.let { c ->
                        startIndex = state.programs.indexOf(c)
                        if (startIndex == -1)
                           startIndex = 0
                     }
                     scope.launch {
                        if (lazyColumnState.firstVisibleItemIndex == 0)
                           lazyColumnState.scrollToItem(if (startIndex < 3) 0 else startIndex - 3)
                     }
                  }
               }

               LazyColumn(state = lazyColumnState) {
                  items(
                     state.programs.size,
                     key = { i ->
                        state.programs[i].id
                     }
                  ) { i ->
                     val program = state.programs[i]
                     val actionBackground = MaterialTheme.colorScheme.secondaryContainer
                     val actionTint = MaterialTheme.colorScheme.onSecondaryContainer

                     SwipeableItemWithActions(
                        isRevealed = program.isOptionsRevealed,
                        onExpanded = {
                           viewModel.replaceItem(i, program.copy(isOptionsRevealed = true))
                        },
                        onCollapsed = {
                           viewModel.replaceItem(i, program.copy(isOptionsRevealed = false))
                        },
                        actions = {
                           ActionIcon(
                              onClick = {
                                 shareProgram(program, context)
                              },
                              backgroundColor = actionBackground,
                              icon = Icons.Default.Share,
                              tint = actionTint,
                              modifier = Modifier.fillMaxHeight()
                           )
                           Spacer(modifier = Modifier.width(2.dp))
                           ActionIcon(
                              onClick = {
                                 addToCal(program, context)
                              },
                              backgroundColor = actionBackground,
                              icon = Icons.Default.Event,
                              tint = actionTint,
                              modifier = Modifier.fillMaxHeight()
                           )
                        }) {
                        ProgramItem(
                           program = program,
                           modifier = Modifier
                              .fillParentMaxWidth(),
                           compact = compact,
                           onItemClicked = {
                              navigator.navigate(ProgramEditScreenDestination(program.id))
                           }
                        )
                     }
                     if (i < state.programs.size - 1) {
                        Spacer(modifier = Modifier.height(6.dp))
                     }
                  }
                  if (state.error.isNotBlank()) {
                     scope.launch {
                        snackbarHostState.showSnackbar(
                           message = state.error, duration = SnackbarDuration.Long
                        )
                     }
                  }
               }
            }
         }

         if (state.programs.isEmpty()) {
            Text(
               text = stringResource(id = R.string.program_no_entries),
               color = MaterialTheme.colorScheme.primary,
               style = MaterialTheme.typography.bodyLarge,
               modifier = Modifier
                  .fillMaxWidth()
                  .padding(horizontal = 20.dp)
                  .align(Alignment.Center),
               textAlign = TextAlign.Center
            )
         }

         if (state.isLoading) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
         }

         state.fetchProgramResult?.let { result ->
            scope.launch {
               snackbarHostState.showSnackbar(
                  message = context.getString(R.string.fetch_result, result.countNew, result.countUpdate)
               )
               viewModel.onEvent(ProgramListEvent.ResetFetchResult)
            }
         }
      }
   }
}

private fun shareProgram(p: Program, context: Context) {
   val text = p.toString()
   val sendIntent = Intent(Intent.ACTION_SEND).apply {
      putExtra(Intent.EXTRA_TEXT, text)
      type = "text/plain"
   }
   val shareIntent = Intent.createChooser(sendIntent, null)
   context.startActivity(shareIntent, null)
}

private fun addToCal(p: Program, context: Context) {
   val startMillis: Long = getOffsetDateTimeInMillis(p.date, LocalTime.of(19, 30))
   val endMillis: Long = getOffsetDateTimeInMillis(p.date, LocalTime.of(21, 0))
   val title = context.getString(R.string.cal_title, p.topic)
   val location = context.getString(R.string.cal_location)
   val description = context.getString(R.string.app_name)

   val intent = Intent(Intent.ACTION_INSERT)
      .setData(CalendarContract.Events.CONTENT_URI)
      .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
      .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
      .putExtra(CalendarContract.Events.CALENDAR_TIME_ZONE, TimeZone.getDefault())
      .putExtra(CalendarContract.Events.TITLE, title)
      .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
      .putExtra(
         CalendarContract.Events.AVAILABILITY,
         CalendarContract.Events.AVAILABILITY_BUSY
      )
      .putExtra(CalendarContract.Events.DESCRIPTION, description)
   context.startActivity(intent)
}