/*
 * FileRepository.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.domain.repository

import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo
import de.micmun.android.miwotreff.util.Resource
import kotlinx.coroutines.flow.Flow

/**
 * Interface for file repository.
 *
 * @author MicMun
 * @version 1.0, 14.09.24
 */
interface FileRepository {
   fun getFiles(): Flow<Resource<List<FileInfo>>>
   suspend fun backup(): Flow<Resource<String>>
   suspend fun restore(fileInfo: FileInfo): Flow<Resource<Int>>
   suspend fun deleteFiles(filesToHold: Int): Flow<Resource<Int>>
}
