package de.micmun.android.miwotreff.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.micmun.android.miwotreff.feature_backup.data.repository.FileRepositoryImpl
import de.micmun.android.miwotreff.feature_backup.domain.repository.FileRepository
import de.micmun.android.miwotreff.feature_program.data.repository.ProgramRepositoryImpl
import de.micmun.android.miwotreff.feature_program.data.repository.SettingRepositoryImpl
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.feature_program.domain.repository.SettingRepository
import javax.inject.Singleton

/**
 * Binds module for interfaces.
 *
 * @author MicMun
 * @version 1.0, 18.08.24
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
   @Binds
   @Singleton
   abstract fun bindProgramRepository(
      programRepository: ProgramRepositoryImpl
   ): ProgramRepository

   @Binds
   @Singleton
   abstract fun bindSettingRepository(
      settingRepository: SettingRepositoryImpl
   ): SettingRepository

   @Binds
   @Singleton
   abstract fun bindFileRepository(
      fileRepository: FileRepositoryImpl
   ): FileRepository
}
