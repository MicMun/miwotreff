/*
 * FileRepositoryImpl.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.data.repository

import android.app.Application
import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_backup.data.local.FileAccessApi
import de.micmun.android.miwotreff.feature_backup.data.mapper.toFileInfo
import de.micmun.android.miwotreff.feature_backup.data.mapper.toFileInfoDto
import de.micmun.android.miwotreff.feature_backup.data.mapper.toProgramDto
import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo
import de.micmun.android.miwotreff.feature_backup.domain.repository.FileRepository
import de.micmun.android.miwotreff.feature_program.data.mapper.toProgram
import de.micmun.android.miwotreff.feature_program.data.remote.dto.ProgramDto
import de.micmun.android.miwotreff.feature_program.domain.model.FetchProgramResult
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import java.lang.reflect.ParameterizedType
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

/**
 * Repository implementation for file handling.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
class FileRepositoryImpl @Inject constructor(
   private val fileAccessApi: FileAccessApi,
   private val programRepository: ProgramRepository,
   private val application: Application
) : FileRepository {
   private val moshi: Moshi = Moshi.Builder()
      .addLast(KotlinJsonAdapterFactory())
      .build()
   private val listMyData: ParameterizedType =
      Types.newParameterizedType(MutableList::class.java, ProgramDto::class.java)
   private val jsonAdapter: JsonAdapter<List<ProgramDto>> = moshi.adapter(listMyData)

   /**
    * Returns the backup files from folder.
    *
    * @return Flow resource with list of file infos or error message.
    */
   override fun getFiles(): Flow<Resource<List<FileInfo>>> = flow {
      emit(Resource.Loading(isLoading = true))

      try {
         val files = fileAccessApi.getFiles().map { file ->
            file.toFileInfo()
         }
         emit(Resource.Success(data = files))
      } catch (e: Exception) {
         Log.e("FileRepositoryImpl", "Error: ${e.message}")
         e.printStackTrace()
         emit(Resource.Error(message = e.localizedMessage ?: application.getString(R.string.error_unknown)))
      }
      emit(Resource.Loading(isLoading = false))
   }

   /**
    * Creates a backup file.
    */
   override suspend fun backup(): Flow<Resource<String>> = flow {
      emit(Resource.Loading(isLoading = true))
      try {
         val entries = programRepository.searchProgramEntries("").first()
         if (entries.isNotEmpty()) {
            val content = jsonAdapter.toJson(entries.map { it.toProgramDto() })
            val fileName = buildCurrentFileName()
            fileAccessApi.writeFile(fileName, content)
            emit(Resource.Success(data = fileName))
         } else {
            emit(Resource.Error(message = application.getString(R.string.error_no_entries)))
         }
      } catch (e: NoSuchElementException) {
         Log.e("FileRepositoryImpl", "No entries: ${e.message}")
         emit(Resource.Error(message = application.getString(R.string.error_no_entries_msg, e.localizedMessage)))
      } catch (e: Exception) {
         emit(Resource.Error(message = application.getString(R.string.error_message, e.localizedMessage)))
      }
      emit(Resource.Loading(isLoading = false))
   }

   /**
    * Restores data from backup file.
    */
   override suspend fun restore(fileInfo: FileInfo): Flow<Resource<Int>> = flow {
      emit(Resource.Loading(isLoading = true))
      try {
         val content = fileAccessApi.readFile(fileInfo.toFileInfoDto())
         val entries = jsonAdapter.fromJson(content)?.map { it.toProgram() } ?: emptyList()
         val result: FetchProgramResult = if (entries.isNotEmpty()) {
            programRepository.writeBulk(entries)
         } else {
            FetchProgramResult(countNew = 0)
         }
         emit(Resource.Success(data = result.countNew))
      } catch (e: Exception) {
         Log.e("FileRepositoryImpl", "Error: ${e.message}")
         emit(Resource.Error(message = application.getString(R.string.error_message, e.localizedMessage)))
      }
      emit(Resource.Loading(isLoading = false))
   }

   override suspend fun deleteFiles(filesToHold: Int): Flow<Resource<Int>> = flow {
      try {
         val count = fileAccessApi.deleteFiles(filesToHold)
         emit(Resource.Success(data = count))
      } catch (e: Exception) {
         e.printStackTrace()
         emit(Resource.Error(message = application.getString(R.string.error_message, e.localizedMessage)))
      }
   }

   private fun buildCurrentFileName(): String {
      val now = LocalDateTime.now()
      val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
      val prefix = "miwotreff-"
      val suffix = ".json"
      val dateTime = now.format(formatter)

      val last = searchFile(dateTime)
      val fileName = if (last != null) {
         val start = (prefix + dateTime).length
         val end = last.name.indexOf(suffix)
         val numberStr = last.name.substring(start, end)
         val number = if (numberStr.isBlank()) {
            1
         } else {
            numberStr.toInt() + 1
         }
         "$prefix$dateTime-$number$suffix"
      } else {
         "$prefix$dateTime$suffix"
      }

      return fileName
   }

   private fun searchFile(search: String): FileInfo? {
      return fileAccessApi.searchLastFile(search)?.toFileInfo()
   }
}