/*
 * ActionIcon.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.programs.components

import androidx.compose.foundation.background
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * ActionIcon represents on action while swiping.
 *
 * @author MicMun
 * @version 1.0, 26.09.24
 */
@Composable
fun ActionIcon(
   onClick: () -> Unit,
   backgroundColor: Color,
   icon: ImageVector,
   modifier: Modifier = Modifier,
   contentDescription: String? = null,
   tint: Color = MaterialTheme.colorScheme.onSurface
) {
   IconButton(
      onClick = onClick,
      modifier = modifier
         .background(backgroundColor)
   ) {
      Icon(
         imageVector = icon,
         contentDescription = contentDescription,
         tint = tint
      )
   }
}
