package de.micmun.android.miwotreff.feature_program.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import de.micmun.android.miwotreff.feature_program.data.local.DateConverter
import java.time.LocalDate

/**
 * Entity for program.
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
@Entity(
   tableName = "program",
   indices = [Index(value = ["p_date"], unique = true)]
)
@TypeConverters(DateConverter::class)
data class ProgramEntity(
   @PrimaryKey(autoGenerate = true)
   @ColumnInfo(name = "id")
   var id: Int = 0,
   @ColumnInfo(name = "p_date")
   val date: LocalDate,
   @ColumnInfo(name = "p_topic")
   var topic: String = "",
   @ColumnInfo(name = "p_person")
   var person: String? = null,
   @ColumnInfo(name = "edit", defaultValue = "0")
   var edit: Int = 0
)
