/*
 * ProgramRepository.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.domain.repository

import de.micmun.android.miwotreff.feature_program.domain.model.FetchProgramResult
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.util.Resource
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

/**
 * Local data repository interface.
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
interface ProgramRepository {
   fun fetchProgramEntries(): Flow<Resource<FetchProgramResult>>

   fun searchProgramEntries(query: String): Flow<List<Program>>

   suspend fun findProgramById(id: Int): Resource<Program>
   suspend fun findProgramByDate(date: LocalDate): Resource<Program>

   suspend fun updateProgram(program: Program)

   suspend fun writeBulk(entries: List<Program>): FetchProgramResult
}
