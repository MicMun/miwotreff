/*
 * MiwotreffApp.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.anggrayudi.storage.SimpleStorageHelper
import dagger.hilt.android.HiltAndroidApp
import de.micmun.android.miwotreff.feature_program.domain.service.UpdateServiceWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asExecutor
import javax.inject.Inject

/**
 * Application.
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
@HiltAndroidApp
class MiwotreffApp : Application(), Configuration.Provider {
   @Inject
   lateinit var workerFactory: HiltWorkerFactory

   lateinit var storageHelper: SimpleStorageHelper

   override val workManagerConfiguration: Configuration
      get() = Configuration.Builder()
         .setExecutor(Dispatchers.Default.asExecutor())
         .setWorkerFactory(workerFactory)
         .setTaskExecutor(Dispatchers.Default.asExecutor())
         .build()

   override fun onCreate() {
      super.onCreate()
      createNotificationChannel()
   }

   private fun createNotificationChannel() {
      // Create the NotificationChannel
      val channel = NotificationChannel(
         UpdateServiceWorker.CHANNEL_ID,
         getString(R.string.channel_name),
         NotificationManager.IMPORTANCE_DEFAULT
      )
      channel.description = getString(R.string.channel_description)

      // Register the channel with the system.
      val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
      notificationManager.createNotificationChannel(channel)
   }
}
