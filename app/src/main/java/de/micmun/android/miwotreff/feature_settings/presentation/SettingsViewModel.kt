/*
 * SettingsViewModel.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_settings.presentation

import android.app.Application
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.micmun.android.miwotreff.datastore
import de.micmun.android.miwotreff.feature_settings.data.AppSettings
import de.micmun.android.miwotreff.feature_settings.data.ThemeSetting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for settings.
 *
 * @author MicMun
 * @version 1.0, 20.08.24
 */
@HiltViewModel
class SettingsViewModel @Inject constructor(
   val application: Application
) : ViewModel() {
   var appSettings = mutableStateOf(AppSettings())

   init {
      getAppSettings()
   }

   fun changeTheme(themeSetting: ThemeSetting) {
      viewModelScope.launch(Dispatchers.IO) {
         application.datastore.updateData {
            it.copy(theme = themeSetting)
         }
      }
   }

   fun changeAutoSync(autoSync: Boolean) {
      viewModelScope.launch(Dispatchers.IO) {
         application.datastore.updateData {
            it.copy(autosync = autoSync)
         }
      }
   }

   fun changeFilesToHold(filesToHold: Int) {
      viewModelScope.launch(Dispatchers.IO) {
         application.datastore.updateData {
            it.copy(filesToHold = filesToHold)
         }
      }
   }

   private fun getAppSettings() {
      viewModelScope.launch(Dispatchers.IO) {
         application.datastore.data.collect { settings ->
            appSettings.value = settings
         }
      }
   }
}
