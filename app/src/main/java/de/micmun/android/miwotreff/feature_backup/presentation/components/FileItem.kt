/*
 * FileItem.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo
import de.micmun.android.miwotreff.util.toUiTime

/**
 * One item with file to show.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
@Composable
fun FileItem(
   fileInfo: FileInfo,
   modifier: Modifier = Modifier,
   onClick: (FileInfo) -> Unit = {}
) {
   Card(modifier = modifier
      .clickable { onClick(fileInfo) }
   ) {
      Column(
         modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
         horizontalAlignment = Alignment.Start,
         verticalArrangement = Arrangement.Top
      ) {
         Text(
            text = fileInfo.name,
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = FontWeight.Bold
         )
         Spacer(modifier = Modifier.height(2.dp))
         Text(
            text = fileInfo.lastModified.toUiTime(),
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier.padding(horizontal = 8.dp)
         )
      }
   }
}
