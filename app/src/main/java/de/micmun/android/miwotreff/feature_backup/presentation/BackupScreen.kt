/*
 * BackupScreen.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.presentation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo
import de.micmun.android.miwotreff.feature_backup.presentation.components.FileItem
import de.micmun.android.miwotreff.feature_backup.presentation.components.RestoreConfirmDialog
import de.micmun.android.miwotreff.feature_program.presentation.components.AppBarAction
import de.micmun.android.miwotreff.feature_program.presentation.components.ProgramTopBar
import kotlinx.coroutines.launch

/**
 * Screen for backup and restore.
 *
 * @author MicMun
 * @version 1.0, 20.08.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Destination<RootGraph>
@Composable
fun BackupScreen(
   navigator: DestinationsNavigator,
   viewModel: BackupViewModel = hiltViewModel()
) {
   val state = viewModel.state
   val snackbarHostState = remember {
      SnackbarHostState()
   }
   val scope = rememberCoroutineScope()
   val scollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()

   when (state.rootDir.isNotBlank()) {
      false ->
         Scaffold(
            snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
            modifier = Modifier
               .fillMaxSize()
               .nestedScroll(scollBehavior.nestedScrollConnection),
            topBar = {
               ProgramTopBar(
                  title = stringResource(id = R.string.screen_title_backup)
               )
            }
         ) { innerPaddings ->
            Box(
               modifier = Modifier
                  .fillMaxSize()
                  .padding(innerPaddings),
               contentAlignment = Alignment.Center
            ) {
               Column(
                  modifier = Modifier
                     .fillMaxWidth()
                     .padding(horizontal = 16.dp),
                  verticalArrangement = Arrangement.Center,
                  horizontalAlignment = Alignment.CenterHorizontally
               ) {
                  Text(
                     text = stringResource(id = R.string.backup_help_message),
                     style = MaterialTheme.typography.bodyLarge
                  )
                  Button(onClick = {
                     viewModel.openFilePicker()
                  }) {
                     Text(text = stringResource(id = R.string.backup_access_btn))
                  }
               }
               LaunchedEffect(key1 = state.error) {
                  if (state.error.isNotBlank()) {
                     scope.launch {
                        snackbarHostState.showSnackbar(
                           message = state.error, duration = SnackbarDuration.Long
                        )
                     }
                  }
               }
            }
         }

      true ->
         Scaffold(
            topBar = {
               ProgramTopBar(
                  title = stringResource(id = R.string.screen_title_backup),
                  scrollBehavior = scollBehavior,
                  actions = listOf(
                     AppBarAction(
                        title = stringResource(id = R.string.backup_delete_btn),
                        icon = Icons.Default.Delete,
                        enabled = true
                     ) {
                        // delete all files without the last x files
                        viewModel.onEvent(BackupEvent.DeleteFiles)
                     }
                  )
               )
            },
            snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
            modifier = Modifier
               .fillMaxSize()
               .nestedScroll(scollBehavior.nestedScrollConnection),
            floatingActionButton = {
               FloatingActionButton(onClick = {
                  viewModel.onEvent(BackupEvent.AddBackup)
               }) {
                  Icon(
                     imageVector = Icons.Default.Add,
                     contentDescription = "New backup"
                  )
               }
            }
         ) { innerPaddings ->
            Box(
               modifier = Modifier
                  .fillMaxSize()
                  .padding(innerPaddings),
               contentAlignment = Alignment.Center
            ) {
               LazyColumn(
                  modifier = Modifier
                     .fillMaxSize()
                     .padding(vertical = 8.dp)
               ) {
                  items(state.files.size, key = { i ->
                     state.files[i].name
                  }) { i ->
                     val file = state.files[i]
                     val showDialog = remember {
                        mutableStateOf(false)
                     }
                     var fileToRestore by remember<MutableState<FileInfo?>> {
                        mutableStateOf(null)
                     }

                     FileItem(
                        fileInfo = file,
                        modifier = Modifier
                           .fillParentMaxWidth(),
                        onClick = { f ->
                           fileToRestore = f
                           showDialog.value = true
                        }
                     )

                     if (showDialog.value) {
                        RestoreConfirmDialog(
                           question = stringResource(R.string.backup_ask_confirm),
                           confirmText = stringResource(R.string.backup_confirm_text),
                           cancelText = stringResource(R.string.backup_cancel_text),
                           onConfirm = {
                              showDialog.value = false
                              fileToRestore?.let { f ->
                                 viewModel.onEvent(BackupEvent.RestoreBackup(f))
                              }
                           },
                           onDismiss = {
                              showDialog.value = false
                           }
                        )
                     }

                     if (i < state.files.size - 1) {
                        Spacer(modifier = Modifier.height(6.dp))
                     }
                  }
                  if (state.error.isNotBlank()) {
                     scope.launch {
                        snackbarHostState.showSnackbar(
                           message = state.error, duration = SnackbarDuration.Long
                        )
                     }
                  }
               }
               if (state.files.isEmpty()) {
                  Text(
                     text = stringResource(id = R.string.backup_no_files),
                     color = MaterialTheme.colorScheme.primary,
                     style = MaterialTheme.typography.bodyLarge,
                     modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .align(Alignment.Center),
                     textAlign = TextAlign.Center
                  )
               }

               if (state.isLoading) {
                  CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
               }

               state.result?.let { countEntries ->
                  val msg = stringResource(id = R.string.backup_restored, countEntries)
                  scope.launch {
                     viewModel.onEvent(BackupEvent.ResetRestoreCount)
                     snackbarHostState.showSnackbar(message = msg)
                  }
               }

               state.backupedFile?.let { fileName ->
                  val msg = stringResource(id = R.string.backup_created, fileName)
                  scope.launch {
                     viewModel.onEvent(BackupEvent.ResetBackupFile)
                     snackbarHostState.showSnackbar(message = msg, duration = SnackbarDuration.Short)
                  }
               }

               state.filesDeleted?.let { filesDeleted ->
                  if (filesDeleted != -1) {
                     val msg = stringResource(id = R.string.backup_deleted, filesDeleted)
                     scope.launch {
                        viewModel.onEvent(BackupEvent.ResetDeletedFiles)
                        snackbarHostState.showSnackbar(message = msg)
                     }
                  }
               }
            }
         }
   }
}
