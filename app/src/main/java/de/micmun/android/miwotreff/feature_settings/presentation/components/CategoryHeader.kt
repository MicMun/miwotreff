/*
 * CategoryHeader.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_settings.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

/**
 * Header of a settings category.
 *
 * @author MicMun
 * @version 1.0, 27.08.24
 */
@Composable
fun CategoryHeader(
   text: String,
   modifier: Modifier = Modifier
) {
   Text(
      text = text,
      style = MaterialTheme.typography.titleLarge,
      color = MaterialTheme.colorScheme.primary,
      modifier = modifier
         .fillMaxWidth()
         .background(MaterialTheme.colorScheme.onPrimary)
         .padding(16.dp)
   )
}
