/*
 * MiwDateUtils.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.util

import android.util.Log
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.Locale

/**
 * Functions for date formatting.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */

object ProgramFormatter {
   val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.uuuu")
   val timeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.uuuu HH:mm:ss")
}

fun LocalDate.toUiDate(): String {
   return this.format(ProgramFormatter.formatter)
}

fun String.toLocalDateTime(): LocalDate {
   return LocalDate.parse(this, ProgramFormatter.formatter)
}

fun Long.toLocalDateTime(): LocalDateTime {
   val instant = Instant.ofEpochMilli(this)
   val zoneId = ZoneId.systemDefault()
   return instant.atZone(zoneId).toLocalDateTime()
}

fun LocalDateTime.toUiTime(): String {
   return this.format(ProgramFormatter.timeFormatter)
}

fun LocalDateTime.toMillis(): Long {
   val instant = Instant.from(OffsetDateTime.of(this, getZoneOffset()))
   return instant.toEpochMilli()
}

fun getOffsetDateTimeInMillis(date: LocalDate, time: LocalTime): Long {
   val dateTime = OffsetDateTime.of(date, time, getZoneOffset())
   val instant = Instant.from(dateTime)
   return instant.toEpochMilli()
}

private fun getZoneOffset(): ZoneOffset {
   val instant = Instant.now()
   val zoneId = ZoneId.systemDefault()
   return zoneId.rules.getOffset(instant)
}