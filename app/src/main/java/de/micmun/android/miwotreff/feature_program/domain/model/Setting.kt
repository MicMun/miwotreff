package de.micmun.android.miwotreff.feature_program.domain.model

/**
 * Data model for settings.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
data class Setting(
   val key: String,
   val value: String? = null
)
