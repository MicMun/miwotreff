package de.micmun.android.miwotreff.di

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.micmun.android.miwotreff.common.Constants
import de.micmun.android.miwotreff.feature_program.data.local.ProgramDatabase
import de.micmun.android.miwotreff.feature_program.data.remote.ProgramApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import javax.inject.Singleton

/**
 * App module for dependency injection.
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {
   @Provides
   @Singleton
   fun provideProgramApi(): ProgramApi {
      val moshi = Moshi.Builder()
         .addLast(KotlinJsonAdapterFactory())
         .build()
      val okHttpClient = OkHttpClient.Builder()
         .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
         .build()

      return Retrofit.Builder()
         .baseUrl(Constants.BASE_URL)
         .addConverterFactory(MoshiConverterFactory.create(moshi))
         .client(okHttpClient)
         .build()
         .create()
   }

   @Provides
   @Singleton
   fun provideProgramDatabase(app: Application): ProgramDatabase {
      val migration23 = object : Migration(2, 3) {
         override fun migrate(db: SupportSQLiteDatabase) {
            // add unique index
            val crProgam = """
               CREATE TABLE program(
                  id integer primary key autoincrement not null,
                  p_date integer not null unique,
                  p_topic text not null,
                  p_person text,
                  edit integer default 0 not null
               )
            """.trimIndent()
            db.execSQL(crProgam)
            val insProgram = """
               INSERT INTO program(id, p_date, p_topic, p_person, edit)
               SELECT _id, p_date/1000, p_topic, p_person, edit
               FROM programm
            """.trimIndent()
            db.execSQL(insProgram)
            db.execSQL("DROP TABLE programm")
            db.execSQL("CREATE UNIQUE INDEX index_program_p_date ON program(p_date ASC)")
            // make settings.key not nullable
            db.execSQL("DROP TABLE settings")
            db.execSQL("CREATE TABLE settings (key TEXT primary key not null, value TEXT)")
            db.execSQL(
               "INSERT INTO settings VALUES('${Constants.KEY_LAST_UPDATE}', " +
                     "'${Constants.DEFAULT_LAST_UPDATE}')"
            )
         }
      }

      return Room.databaseBuilder(
         app,
         ProgramDatabase::class.java,
         ProgramDatabase.DATABASE_NAME
      )
         .addMigrations(migration23)
         .build()
   }
}
