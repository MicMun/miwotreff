/*
 * FileMapper.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.data.mapper

import de.micmun.android.miwotreff.feature_backup.data.local.model.FileInfoDto
import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo
import de.micmun.android.miwotreff.util.toLocalDateTime
import de.micmun.android.miwotreff.util.toMillis

/**
 * Mapper for File to FileInfo.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
fun FileInfoDto.toFileInfo(): FileInfo {
   return FileInfo(
      name = this.name,
      path = this.path,
      lastModified = this.lastModified.toLocalDateTime()
   )
}

fun FileInfo.toFileInfoDto(): FileInfoDto {
   return FileInfoDto(
      name = this.name,
      path = this.path,
      lastModified = this.lastModified.toMillis()
   )
}
