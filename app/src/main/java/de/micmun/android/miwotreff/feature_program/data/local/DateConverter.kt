/*
 * DateConverter.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.local

import androidx.room.TypeConverter
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Converter for date in database.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
class DateConverter {
   private val offsetId = ZoneId.of("Europe/Berlin")

   @TypeConverter
   fun fromTimestamp(date: Long): LocalDate {
      val zonedDateTime = Instant.ofEpochSecond(date).atZone(offsetId)
      return LocalDate.from(zonedDateTime)
   }

   @TypeConverter
   fun toTimestamp(date: LocalDate): Long {
      val zonedDateTime = ZonedDateTime.of(date, LocalTime.of(0, 0), offsetId)
      return zonedDateTime.toEpochSecond()
   }
}
