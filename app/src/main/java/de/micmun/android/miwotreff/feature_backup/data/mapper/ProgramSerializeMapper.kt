/*
 * ProgramSerializeMapper.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.data.mapper

import de.micmun.android.miwotreff.feature_program.data.remote.dto.ProgramDto
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.util.toUiDate

/**
 * Mappper for program to dto.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
fun Program.toProgramDto(): ProgramDto {
   return ProgramDto(this.date.toUiDate(), this.topic, this.person ?: "")
}
