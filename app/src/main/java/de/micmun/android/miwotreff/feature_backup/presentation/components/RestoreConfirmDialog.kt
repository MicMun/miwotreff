/*
 * RestoreConfirmDialog.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.presentation.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.AlertDialogDefaults
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

/**
 * Dialog for confirm restore backup.
 *
 * @author MicMun
 * @version 1.0, 10.10.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RestoreConfirmDialog(
   question: String,
   confirmText: String = "",
   cancelText: String = "",
   onConfirm: () -> Unit = {},
   onDismiss: () -> Unit = {}
) {
   BasicAlertDialog(
      onDismissRequest = onDismiss
   ) {
      Surface(
         modifier = Modifier
            .wrapContentWidth()
            .wrapContentHeight(),
         shape = MaterialTheme.shapes.large,
         tonalElevation = AlertDialogDefaults.TonalElevation
      ) {
         Column(modifier = Modifier.padding(8.dp)) {
            Text(
               text = question
            )
            Spacer(modifier = Modifier.height(24.dp))
            Row(
               modifier = Modifier.fillMaxWidth(),
               horizontalArrangement = Arrangement.SpaceAround,
               verticalAlignment = Alignment.CenterVertically
            ) {
               TextButton(
                  onClick = onConfirm
               ) {
                  Text(confirmText)
               }
               TextButton(
                  onClick = onDismiss
               ) {
                  Text(cancelText)
               }
            }
         }
      }
   }
}
