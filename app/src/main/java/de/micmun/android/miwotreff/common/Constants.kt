package de.micmun.android.miwotreff.common

/**
 * Constants for the app.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */
object Constants {
   const val BASE_URL = "https://mittwochstreff-muenchen.de/"
   const val PARAM_PROGRAM_ID = "programId"
   const val FILES_TO_HOLD = 4
   const val KEY_LAST_UPDATE = "last_update"
   const val DEFAULT_LAST_UPDATE = "01.01.2024"
}
