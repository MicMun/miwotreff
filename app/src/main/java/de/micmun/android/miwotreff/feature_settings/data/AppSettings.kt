/*
 * AppSettings.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_settings.data

import de.micmun.android.miwotreff.common.Constants
import kotlinx.serialization.Serializable

/**
 * Settings for the app.
 *
 * @author MicMun
 * @version 1.0, 27.08.24
 */
@Serializable
data class AppSettings(
   val theme: ThemeSetting = ThemeSetting.SYSTEM,
   val autosync: Boolean = true,
   val filesToHold: Int = Constants.FILES_TO_HOLD,
   val rootDir: String = ""
)

enum class ThemeSetting {
   SYSTEM, LIGHT, DARK, DYNAMICLIGHT, DYNAMICDARK
}
