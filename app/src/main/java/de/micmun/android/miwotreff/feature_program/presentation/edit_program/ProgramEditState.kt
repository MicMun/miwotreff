package de.micmun.android.miwotreff.feature_program.presentation.edit_program

import de.micmun.android.miwotreff.feature_program.domain.model.Program

/**
 * State for edit screen.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */
data class ProgramEditState(
   val program: Program? = null,
   val error: String = "",
   val isLoading: Boolean = false
)
