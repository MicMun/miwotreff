package de.micmun.android.miwotreff

import android.Manifest
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TooltipBox
import androidx.compose.material3.TooltipDefaults
import androidx.compose.material3.adaptive.currentWindowAdaptiveInfo
import androidx.compose.material3.adaptive.navigationsuite.NavigationSuiteScaffold
import androidx.compose.material3.adaptive.navigationsuite.NavigationSuiteScaffoldDefaults
import androidx.compose.material3.adaptive.navigationsuite.NavigationSuiteType
import androidx.compose.material3.rememberTooltipState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.datastore.dataStore
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.navigation.compose.rememberNavController
import androidx.window.core.layout.WindowHeightSizeClass
import androidx.window.core.layout.WindowWidthSizeClass
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.anggrayudi.storage.SimpleStorageHelper
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.rememberPermissionState
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.generated.NavGraphs
import com.ramcosta.composedestinations.utils.toDestinationsNavigator
import dagger.hilt.android.AndroidEntryPoint
import de.micmun.android.miwotreff.feature_program.domain.service.UpdateServiceWorker
import de.micmun.android.miwotreff.feature_settings.data.AppSettings
import de.micmun.android.miwotreff.feature_settings.data.AppSettingsSerializer
import de.micmun.android.miwotreff.feature_settings.data.ThemeSetting
import de.micmun.android.miwotreff.ui.theme.MiwotreffTheme
import de.micmun.android.miwotreff.util.Screen
import java.time.Duration
import java.time.temporal.ChronoUnit

val Context.datastore by dataStore("app-settings.json", AppSettingsSerializer)

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
   companion object {
      const val WORK_NAME = "program-update"
   }

   private lateinit var storage: SimpleStorageHelper

   @OptIn(ExperimentalPermissionsApi::class)
   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
      enableEdgeToEdge()
      (application as MiwotreffApp).storageHelper = SimpleStorageHelper(this)
      storage = (application as MiwotreffApp).storageHelper

      setContent {
         val permissionState = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            rememberPermissionState(permission = Manifest.permission.POST_NOTIFICATIONS)
         } else {
            object : PermissionState {
               override val hasPermission: Boolean
                  get() = true
               override val permission: String
                  get() = "android. permission. POST_NOTIFICATIONS"
               override val permissionRequested: Boolean
                  get() = true
               override val shouldShowRationale: Boolean
                  get() = false

               override fun launchPermissionRequest() {
               }
            }
         }
         val lifecycleOwner = LocalLifecycleOwner.current
         DisposableEffect(key1 = lifecycleOwner) {
            val observer = LifecycleEventObserver { _, event ->
               if (event == Lifecycle.Event.ON_RESUME) {
                  permissionState.launchPermissionRequest()
               }
            }
            lifecycleOwner.lifecycle.addObserver(observer)

            onDispose {
               lifecycleOwner.lifecycle.removeObserver(observer)
            }
         }
         val appSettings = datastore.data.collectAsState(
            initial = AppSettings()
         )
         val systemDark = isSystemInDarkTheme()

         val darkTheme = when (appSettings.value.theme) {
            ThemeSetting.DARK, ThemeSetting.DYNAMICDARK -> true
            ThemeSetting.LIGHT, ThemeSetting.DYNAMICLIGHT -> false
            else -> systemDark
         }

         val dynamicTheme = when (appSettings.value.theme) {
            ThemeSetting.DYNAMICDARK, ThemeSetting.DYNAMICLIGHT -> true
            ThemeSetting.SYSTEM -> true
            else -> false
         }

         initWorker(appSettings.value.autosync) // sync service

         MiwotreffTheme(darkTheme = darkTheme, dynamicColor = dynamicTheme) {
            Surface(
               modifier = Modifier.fillMaxSize(),
               color = MaterialTheme.colorScheme.background
            ) {
               Content()
            }
         }
      }
   }

   @OptIn(ExperimentalMaterial3Api::class)
   @Composable
   fun Content() {
      var selectedScreenIndex by rememberSaveable {
         mutableIntStateOf(0)
      }
      val navController = rememberNavController()
      val windowWidthClass = currentWindowAdaptiveInfo().windowSizeClass.windowWidthSizeClass
      val windowHeightClass = currentWindowAdaptiveInfo().windowSizeClass.windowHeightSizeClass

      NavigationSuiteScaffold(
         navigationSuiteItems = {
            val navigator = navController.toDestinationsNavigator()

            Screen.entries.forEachIndexed { index, screen ->
               item(
                  selected = index == selectedScreenIndex,
                  onClick = {
                     selectedScreenIndex = index
                     if (screen == Screen.PROGRAM_LIST_SCREEN)
                        navigator.navigate(screen.direction)
                     navigator.navigate(screen.direction)
                  },
                  icon = {
                     TooltipBox(
                        positionProvider = TooltipDefaults.rememberPlainTooltipPositionProvider(),
                        tooltip = {
                           Text(text = stringResource(id = screen.tooltipId))
                        },
                        state = rememberTooltipState()
                     ) {
                        if (index == selectedScreenIndex) {
                           Icon(
                              imageVector = screen.selectedIcon,
                              contentDescription = stringResource(id = screen.titleId)
                           )
                        } else {
                           Icon(
                              imageVector = screen.unselectedIcon,
                              contentDescription = stringResource(id = screen.titleId)
                           )
                        }
                     }
                  },
                  label = {
                     Text(text = stringResource(id = screen.titleId))
                  }
               )
            }
         },
         layoutType = if (windowWidthClass == WindowWidthSizeClass.EXPANDED && windowHeightClass == WindowHeightSizeClass.EXPANDED) {
            NavigationSuiteType.NavigationDrawer
         } else {
            NavigationSuiteScaffoldDefaults.calculateFromAdaptiveInfo(
               currentWindowAdaptiveInfo()
            )
         },
      ) {
         DestinationsNavHost(navGraph = NavGraphs.root, navController = navController)
      }
   }

   @Preview(showBackground = true, showSystemUi = true)
   @Composable
   fun GreetingPreview() {
      Content()
   }

   private fun initWorker(autosync: Boolean) {
      val workManager = WorkManager.getInstance(applicationContext)
      val duration = Duration.of(12, ChronoUnit.HOURS)

      if (autosync) {
         val builder = PeriodicWorkRequestBuilder<UpdateServiceWorker>(duration)
         val request = builder
            .setConstraints(
               Constraints(
                  requiredNetworkType = NetworkType.CONNECTED,
                  requiresBatteryNotLow = true
               )
            )
            .build()

         workManager.enqueueUniquePeriodicWork(
            WORK_NAME,
            ExistingPeriodicWorkPolicy.UPDATE,
            request
         )
      } else {
         workManager.cancelUniqueWork(WORK_NAME)
      }
   }

   override fun onRestoreInstanceState(savedInstanceState: Bundle) {
      super.onRestoreInstanceState(savedInstanceState)
      storage.onRestoreInstanceState(savedInstanceState)
   }

   override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
      storage.onSaveInstanceState(outState)
      super.onSaveInstanceState(outState, outPersistentState)
   }
}
