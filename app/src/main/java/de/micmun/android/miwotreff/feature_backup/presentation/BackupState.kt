package de.micmun.android.miwotreff.feature_backup.presentation

import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo

/**
 * State for backup screen.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
data class BackupState(
   val files: List<FileInfo> = emptyList(),
   val isLoading: Boolean = false,
   val error: String = "",
   val result: Int? = null,
   val backupedFile: String? = null,
   val rootDir: String = "",
   val filesDeleted: Int? = null
)
