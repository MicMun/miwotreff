package de.micmun.android.miwotreff.feature_backup.data.local.model

/**
 * DTO for file info.
 *
 * @author MicMun
 * @version 1.0, 21.09.24
 */
data class FileInfoDto(
   val name: String,
   val path: String,
   val lastModified: Long
)
