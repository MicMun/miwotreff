/*
 * UriSerializer.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_settings.data

import android.net.Uri
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

/**
 * Serializer for Uri.
 *
 * @author MicMun
 * @version 1.0, 21.09.24
 */
object UriSerializer : KSerializer<Uri?> {
   override val descriptor: SerialDescriptor
      get() = PrimitiveSerialDescriptor("Uri?", PrimitiveKind.STRING)

   override
   fun deserialize(decoder: Decoder): Uri? {
      val str = decoder.decodeString()
      return if (str.isBlank()) null else Uri.parse(decoder.decodeString())
   }

   override fun serialize(encoder: Encoder, value: Uri?) {
      val str = value?.toString() ?: ""
      encoder.encodeString(str)
   }

}
