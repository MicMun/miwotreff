/*
 * ProgramEditViewModel.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.edit_program

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.micmun.android.miwotreff.common.Constants
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.util.Resource
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for edit screen.
 *
 * @author MicMun
 * @version 1.0, 10.08.24
 */
@HiltViewModel
class ProgramEditViewModel @Inject constructor(
   savedStateHandle: SavedStateHandle,
   private val repository: ProgramRepository
) : ViewModel() {
   var state by mutableStateOf(ProgramEditState())

   init {
      viewModelScope.launch {
         val programId = savedStateHandle.get<Int>(Constants.PARAM_PROGRAM_ID) ?: return@launch
         state = state.copy(isLoading = true)
         when (val programResult = repository.findProgramById(programId)) {
            is Resource.Success -> {
               state = state.copy(
                  program = programResult.data,
                  isLoading = false,
                  error = ""
               )
            }

            is Resource.Error -> {
               state = state.copy(
                  isLoading = false,
                  error = programResult.message ?: "",
                  program = null
               )
            }

            else -> Unit
         }

      }

   }

   fun saveProgram(program: Program) {
      // Usee case for saving the update
      viewModelScope.launch {
         repository.updateProgram(program)
      }
   }
}
