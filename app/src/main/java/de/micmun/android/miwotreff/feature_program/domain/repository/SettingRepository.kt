/*
 * SettingRepository.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.domain.repository

import de.micmun.android.miwotreff.feature_program.domain.model.Setting
import kotlinx.coroutines.flow.Flow

/**
 * Interface for setting repository.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
interface SettingRepository {
   fun getSettings(): Flow<List<Setting>>

   suspend fun getSetting(key: String): Setting?

   suspend fun insertSetting(setting: Setting)
}
