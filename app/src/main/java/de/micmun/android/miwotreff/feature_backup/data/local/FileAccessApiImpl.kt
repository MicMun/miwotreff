/*
 * FileAccessApiImpl.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.data.local

import android.app.Application
import com.anggrayudi.storage.file.DocumentFileCompat
import com.anggrayudi.storage.file.getAbsolutePath
import com.anggrayudi.storage.file.makeFile
import com.anggrayudi.storage.file.openInputStream
import com.anggrayudi.storage.file.openOutputStream
import de.micmun.android.miwotreff.datastore
import de.micmun.android.miwotreff.feature_backup.data.local.model.FileInfoDto
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.BufferedWriter
import java.util.stream.Collectors
import javax.inject.Inject

/**
 * API for writing and reading files.
 *
 * @author MicMun
 * @version 1.0, 14.09.24
 */
class FileAccessApiImpl @Inject constructor(
   private val application: Application
) : FileAccessApi {
   private var fileDir: String = ""

   init {
      val scope = CoroutineScope(Dispatchers.IO)
      scope.launch {
         application.datastore.data.collectLatest { settings ->
            fileDir = settings.rootDir
         }
      }
   }

   override fun readFile(file: FileInfoDto): String {
      val dfile = DocumentFileCompat.fromFullPath(application, file.path, requiresWriteAccess = true)
      var content = ""
      dfile?.let { f ->
         f.openInputStream(application)?.use { inputStream ->
            BufferedReader(inputStream.bufferedReader()).use { reader ->
               content = reader.readText()
            }
         }
      }

      return content
   }

   override fun writeFile(fileName: String, content: String) {
      val folderOpt = DocumentFileCompat.fromFullPath(application, fileDir, requiresWriteAccess = true)
      folderOpt?.let { folder ->
         val file = folder.makeFile(application, fileName, "application/json")
         file?.let { f ->
            f.openOutputStream(application, append = false)?.use { outputStream ->
               BufferedWriter(outputStream.bufferedWriter()).use { writer ->
                  writer.write(content)
               }
            }
         }
      }
   }

   override fun getFiles(): List<FileInfoDto> {
      val result = mutableListOf<FileInfoDto>()

      val folder = DocumentFileCompat.fromFullPath(application, fileDir, requiresWriteAccess = true)
      folder?.listFiles()?.let { files ->
         files.forEach { file ->
            if (file.isFile && (file.name?.startsWith("miwotreff_") == true || file.name?.startsWith("miwotreff-") == true)) {
               val name = file.name
               val path = file.getAbsolutePath(application)
               val lastModified = file.lastModified()
               name?.let { n ->
                  result.add(FileInfoDto(n, path, lastModified))
               }
            }
         }
      }

      return result.sortedByDescending { it.lastModified }
   }

   override fun searchLastFile(search: String): FileInfoDto? {
      return getFiles()
         .sortedByDescending {
            it.lastModified
         }
         .find {
            it.name.contains(search)
         }
   }

   override fun deleteFiles(fth: Int): Int {
      val files = getFiles().stream().skip(fth.toLong()).collect(Collectors.toList())
      if (files.isNotEmpty()) {
         var count = 0
         files.forEach { file ->
            val df = DocumentFileCompat.fromFullPath(application, file.path, requiresWriteAccess = true)
            df?.let { documentFile ->
               if (documentFile.delete())
                  count++
            }
         }
         return count
      }
      return 0
   }
}
