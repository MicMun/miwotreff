/*
 * FileApiModule.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.micmun.android.miwotreff.feature_backup.data.local.FileAccessApi
import de.micmun.android.miwotreff.feature_backup.data.local.FileAccessApiImpl
import javax.inject.Singleton

/**
 * Binds file api.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class FileApiModule {
   @Binds
   @Singleton
   abstract fun bindFileAccessApi(api: FileAccessApiImpl): FileAccessApi
}
