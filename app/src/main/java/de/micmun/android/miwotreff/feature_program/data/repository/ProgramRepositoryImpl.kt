/*
 * ProgramRepositoryImpl.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.repository

import android.app.Application
import android.util.Log
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_program.data.local.ProgramDatabase
import de.micmun.android.miwotreff.feature_program.data.local.entities.ProgramEntity
import de.micmun.android.miwotreff.feature_program.data.mapper.toProgram
import de.micmun.android.miwotreff.feature_program.data.mapper.toProgramEntity
import de.micmun.android.miwotreff.feature_program.data.remote.ProgramApi
import de.micmun.android.miwotreff.feature_program.domain.model.FetchProgramResult
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.util.Resource
import de.micmun.android.miwotreff.util.toUiDate
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import retrofit2.HttpException
import java.io.IOException
import java.time.LocalDate
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Implementation of program repository.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
@Singleton
class ProgramRepositoryImpl @Inject constructor(
   private val api: ProgramApi,
   private val application: Application,
   db: ProgramDatabase
) : ProgramRepository {
   private val dao = db.programDao

   override fun fetchProgramEntries(): Flow<Resource<FetchProgramResult>> {
      return flow {
         emit(Resource.Loading(true))
         val localPrograms = dao.getAllPrograms()

         val dateVon = if (localPrograms.isEmpty())
            LocalDate.now().toUiDate()
         else
            localPrograms[0].toProgram().date.toUiDate()


         val remotePrograms = try {
            api.getProgramEntries(dateVon).map { it.toProgram() }
         } catch (e: IOException) {
            e.printStackTrace()
            emit(Resource.Loading(false))
            emit(
               Resource.Error(
                  message = application.getString(R.string.repo_error_reach_server, e.localizedMessage)
               )
            )
            null
         } catch (e: HttpException) {
            e.printStackTrace()
            emit(Resource.Loading(false))
            emit(
               Resource.Error(
                  message = application.getString(R.string.repo_error_load_data, e.code(), e.localizedMessage)
               )
            )
            null
         }

         remotePrograms?.let { programs ->
            emit(Resource.Success(saveProgram(programs)))
            emit(Resource.Loading(false))
         }
      }
   }

   override fun searchProgramEntries(query: String): Flow<List<Program>> {
      return dao.searchProgramEntries(query).map { list: List<ProgramEntity> ->
         list.map { entity ->
            entity.toProgram()
         }
      }
   }

   override suspend fun findProgramById(id: Int): Resource<Program> {
      val program = dao.getProgramById(id)?.toProgram()
      return if (program != null)
         Resource.Success(data = program)
      else
         Resource.Error(message = application.getString(R.string.repo_error_no_program_id))
   }

   override suspend fun findProgramByDate(date: LocalDate): Resource<Program> {
      val program = dao.getProgramByDate(date)?.toProgram()
      return if (program != null)
         Resource.Success(data = program)
      else
         Resource.Error(message = application.getString(R.string.repo_error_no_program_date))
   }

   override suspend fun updateProgram(program: Program) {
      dao.getProgramById(program.id)?.let { entity ->
         entity.topic = program.topic
         entity.person = program.person
         entity.edit = 1
         dao.updateProgram(entity)
      }
   }

   override suspend fun writeBulk(entries: List<Program>): FetchProgramResult {
      return saveProgram(entries, overwrite = false)
   }

   private suspend fun saveProgram(
      programs: List<Program>,
      overwrite: Boolean = true
   ): FetchProgramResult {
      val result = FetchProgramResult(countNew = 0, countUpdate = 0)

      programs.forEach { p ->
         val saved = dao.getProgramByDate(p.date)


         if (saved == null) {
            val id = dao.insertProgram(p.toProgramEntity())
            if (id != -1L) {
               result.countNew++
            }
         } else if (saved.edit == 0 && overwrite) {
            if (saved.topic != p.topic || saved.person != p.person) {
               saved.topic = p.topic
               saved.person = p.person
               saved.edit = 0
               dao.updateProgram(saved)
               result.countUpdate++
            }
         }
      }
      Log.i("ProgramRepositoryImpl", "FetchResult: $result")

      return result
   }
}
