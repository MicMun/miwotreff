/*
 * ProgramDao.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import de.micmun.android.miwotreff.feature_program.data.local.entities.ProgramEntity
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

/**
 * Dao for program in database.
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
@Dao
interface ProgramDao {
   @Query("""
      SELECT * 
      FROM program 
      WHERE LOWER(p_topic) LIKE '%' || LOWER(:query) || '%' OR
         LOWER(p_person) LIKE '%' || LOWER(:query) || '%'
      ORDER BY p_date DESC"""
   )
   fun searchProgramEntries(query: String): Flow<List<ProgramEntity>>

   @Query("""
      SELECT * 
      FROM program
      ORDER BY p_date DESC"""
   )
   suspend fun getAllPrograms(): List<ProgramEntity>

   @Query("SELECT * FROM program WHERE id = :id")
   suspend fun getProgramById(id: Int): ProgramEntity?

   @Query("SELECT * FROM program WHERE p_date = :date")
   suspend fun getProgramByDate(date: LocalDate): ProgramEntity?

   @Query("SELECT * FROM program WHERE p_date = (SELECT MAX(p_date) FROM program)")
   suspend fun getLastDate(): ProgramEntity?

   @Insert(onConflict = OnConflictStrategy.IGNORE)
   suspend fun insertProgram(program: ProgramEntity): Long

   @Update
   suspend fun updateProgram(program: ProgramEntity)
}
