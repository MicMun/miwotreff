/*
 * CustomIconButton.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.components

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TooltipBox
import androidx.compose.material3.TooltipDefaults
import androidx.compose.material3.rememberTooltipState
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * An icon button with tooltip.
 *
 * @author MicMun
 * @version 1.0, 24.08.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomIconButton(
   icon: ImageVector,
   description: String? = null,
   tooltip: String = "",
   enabled: Boolean = true,
   onClicked: () -> Unit = {}
) {
   TooltipBox(
      positionProvider = TooltipDefaults.rememberPlainTooltipPositionProvider(),
      tooltip = {
         Text(text = tooltip)
      },
      state = rememberTooltipState()
   ) {
      IconButton(onClick = { onClicked() }, enabled = enabled) {
         Icon(
            imageVector = icon,
            contentDescription = description
         )
      }
   }
}
