/*
 * CategoryItem.kt
 *
 * Copyright 2024 by MicMun
 */

package de.micmun.android.miwotreff.feature_settings.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_settings.data.ThemeSetting

/**
 * Items of app setting.
 *
 * @author MicMun
 * @version 1.0, 27.08.24
 */

@Composable
fun ThemeItem(
   text: String,
   option: ThemeSetting,
   selected: Boolean,
   modifier: Modifier = Modifier,
   onSelected: (ThemeSetting) -> Unit = {}
) {
   Row(
      modifier = modifier
         .clickable {
            onSelected(option)
         },
      horizontalArrangement = Arrangement.Start,
      verticalAlignment = Alignment.CenterVertically
   ) {
      RadioButton(
         selected = selected,
         onClick = { onSelected(option) }
      )
      Spacer(modifier = Modifier.width(2.dp))
      Text(text = text, modifier = Modifier.padding(2.dp))
   }
}

@Composable
fun AutoSyncItem(
   text: String,
   active: Boolean,
   modifier: Modifier = Modifier,
   onSwitch: (Boolean) -> Unit = {}
) {
   Row(
      modifier = modifier
         .clickable {
            onSwitch(!active)
         },
      horizontalArrangement = Arrangement.SpaceBetween,
      verticalAlignment = Alignment.CenterVertically
   ) {
      Text(
         text = text, modifier = Modifier
            .padding(2.dp)
            .weight(1.0f)
      )
      Spacer(modifier = Modifier.width(2.dp))
      Switch(
         checked = active,
         onCheckedChange = {
            onSwitch(it)
         })
   }
}

@Composable
fun FilesToHoldItem(
   text: String,
   nrFiles: Int,
   modifier: Modifier = Modifier,
   onCountChanged: (Int) -> Unit = {}
) {
   Row(
      modifier = modifier,
      horizontalArrangement = Arrangement.SpaceBetween,
      verticalAlignment = Alignment.CenterVertically
   ) {
      Text(
         text = text, modifier = Modifier
            .padding(2.dp)
            .weight(1.0f)
      )
      Spacer(modifier = Modifier.width(2.dp))
      OutlinedTextField(
         value = if (nrFiles == -1) "" else nrFiles.toString(),
         onValueChange = {
            if (it.isNotBlank())
               onCountChanged(it.toInt())
            else
               onCountChanged(-1)
         },
         singleLine = true,
         keyboardOptions = KeyboardOptions.Default.copy(
            keyboardType = KeyboardType.Number
         ),
         suffix = {
            Text(text = stringResource(id = R.string.setting_file_suffix))
         },
         modifier = Modifier
            .weight(0.4f)
            .padding(2.dp)
      )
   }
}
