package de.micmun.android.miwotreff.feature_program.data.remote.dto


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LastUpdateDto(
   @Json(name = "p_date")
   val pDate: String
) {
   override fun toString(): String {
      return pDate
   }
}
