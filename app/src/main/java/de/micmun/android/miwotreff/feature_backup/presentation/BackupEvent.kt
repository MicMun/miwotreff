/*
 * BackupEvent.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.presentation

import de.micmun.android.miwotreff.feature_backup.domain.model.FileInfo

/**
 * Events for backup.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
sealed class BackupEvent {
   object AddBackup : BackupEvent()
   data class RestoreBackup(val file: FileInfo) : BackupEvent()
   object ResetBackupFile : BackupEvent()
   object ResetRestoreCount : BackupEvent()
   object DeleteFiles : BackupEvent()
   object ResetDeletedFiles: BackupEvent()
}
