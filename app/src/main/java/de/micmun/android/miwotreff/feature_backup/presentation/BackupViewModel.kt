/*
 * BackupViewModel.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.presentation

import android.app.Application
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anggrayudi.storage.file.DocumentFileCompat
import com.anggrayudi.storage.file.FileFullPath
import com.anggrayudi.storage.file.StorageId
import com.anggrayudi.storage.file.StorageType
import com.anggrayudi.storage.file.getAbsolutePath
import com.anggrayudi.storage.file.isWritable
import dagger.hilt.android.lifecycle.HiltViewModel
import de.micmun.android.miwotreff.MiwotreffApp
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.datastore
import de.micmun.android.miwotreff.feature_backup.domain.repository.FileRepository
import de.micmun.android.miwotreff.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model for backup screen.
 *
 * @author MicMun
 * @version 1.0, 20.08.24
 */
@HiltViewModel
class BackupViewModel @Inject constructor(
   private val fileRepository: FileRepository,
   private val application: Application
) : ViewModel() {
   var state by mutableStateOf(BackupState())
   private val storage = (application as MiwotreffApp).storageHelper
   private var filesToHold: Int = -1

   init {
      fetchAppSettings()

      storage.onFolderSelected = { _, folder ->
         val folderPath = folder.getAbsolutePath(application)
         Log.i("BackupViewModel", "backup folder = $folderPath")
         updateRootUri(folderPath)
      }
   }

   fun onEvent(event: BackupEvent) {
      when (event) {
         is BackupEvent.AddBackup -> {
            viewModelScope.launch {
               fileRepository.backup().collect { result ->
                  when (result) {
                     is Resource.Success -> {
                        result.data?.let { fileName ->
                           state = state.copy(backupedFile = fileName)
                        }
                        fetchFiles()
                     }

                     is Resource.Error -> {
                        result.message?.let { error ->
                           state = state.copy(error = error)
                        }
                     }

                     is Resource.Loading -> {
                        state = state.copy(isLoading = result.isLoading)
                     }
                  }
               }
            }
         }

         is BackupEvent.RestoreBackup -> {
            val file = event.file
            viewModelScope.launch(Dispatchers.IO) {
               fileRepository.restore(file).collect { result ->
                  when (result) {
                     is Resource.Success -> {
                        result.data?.let { count ->
                           state = state.copy(result = count)
                        }
                     }

                     is Resource.Error -> {
                        result.message?.let { error ->
                           state = state.copy(error = error)
                        }
                     }

                     is Resource.Loading -> {
                        state = state.copy(isLoading = result.isLoading)
                     }
                  }
               }
            }
         }

         is BackupEvent.DeleteFiles -> {
            viewModelScope.launch(Dispatchers.IO) {
               fileRepository.deleteFiles(filesToHold).collect { result ->
                  when (result) {
                     is Resource.Success -> {
                        state = state.copy(filesDeleted = result.data ?: -1)
                        fetchFiles()
                     }

                     is Resource.Error -> {
                        state = state.copy(
                           error = result.message ?: application.getString(R.string.error_unknown),
                           filesDeleted = null
                        )
                     }

                     is Resource.Loading -> {

                     }
                  }
               }
            }
         }

         is BackupEvent.ResetBackupFile -> {
            state = state.copy(backupedFile = null)
         }

         is BackupEvent.ResetRestoreCount -> {
            state = state.copy(result = null)
         }

         is BackupEvent.ResetDeletedFiles -> {
            state = state.copy(filesDeleted = null)
         }
      }
   }

   fun openFilePicker() {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
         storage.openFolderPicker(
            initialPath = FileFullPath(
               context = application,
               StorageType.EXTERNAL,
               "miwotreff"
            )
         )
      } else {
         openPickerOld()
      }
   }

   private fun openPickerOld() {
      storage.openFolderPicker(
         initialPath = FileFullPath(
            context = application,
            StorageId.PRIMARY,
            "miwotreff"
         )
      )
   }

   private fun updateRootUri(rootDir: String) {
      viewModelScope.launch(Dispatchers.IO) {
         application.datastore.updateData {
            it.copy(rootDir = rootDir)
         }
      }
   }

   private fun fetchFiles() {
      viewModelScope.launch(Dispatchers.IO) {
         fileRepository.getFiles().collect { result ->
            when (result) {
               is Resource.Success -> {
                  result.data?.let { files ->
                     state = state.copy(files = files)
                  }
               }

               is Resource.Error -> {
                  result.message?.let { error ->
                     state = state.copy(error = error)
                  }
               }

               is Resource.Loading -> {
                  state = state.copy(isLoading = result.isLoading)
               }
            }
         }
      }
   }

   private fun fetchAppSettings() {
      application.datastore.data
         .onEach { appSettings ->
            state = state.copy(rootDir = appSettings.rootDir)
            filesToHold = appSettings.filesToHold
            if (appSettings.rootDir.isNotBlank()) {
               if (!checkFolderAccessable(application, appSettings.rootDir)) {
                  state = state.copy(
                     error = application.getString(R.string.error_no_access, appSettings.rootDir),
                     rootDir = ""
                  )
               } else {
                  fetchFiles()
               }
            }
         }
         .launchIn(viewModelScope)
   }

   private fun checkFolderAccessable(context: Context, rootDir: String): Boolean {
      val grantedPaths = DocumentFileCompat.getAccessibleAbsolutePaths(context)
      val paths = grantedPaths.values.find { it.contains(rootDir) } ?: emptySet()
      if (paths.isNotEmpty() && paths.size == 1) {
         val treffer = paths.toList()
         DocumentFileCompat
            .fromFullPath(context, treffer[0], requiresWriteAccess = true)?.let { folder ->
               if (folder.isDirectory && folder.isWritable(context))
                  return true
            }
      }

      return false
   }
}
