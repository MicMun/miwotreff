/*
 * SettingsScreen.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_settings.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootGraph
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.feature_program.presentation.components.ProgramTopBar
import de.micmun.android.miwotreff.feature_settings.data.AppSettings
import de.micmun.android.miwotreff.feature_settings.data.ThemeSetting
import de.micmun.android.miwotreff.feature_settings.presentation.components.AutoSyncItem
import de.micmun.android.miwotreff.feature_settings.presentation.components.CategoryHeader
import de.micmun.android.miwotreff.feature_settings.presentation.components.FilesToHoldItem
import de.micmun.android.miwotreff.feature_settings.presentation.components.ThemeItem

/**
 * Screen for settings.
 *
 * @author MicMun
 * @version 1.0, 20.08.24
 */
@OptIn(ExperimentalMaterial3Api::class)
@Destination<RootGraph>
@Composable
fun SettingsScreen(
   viewModel: SettingsViewModel = hiltViewModel()
) {
   Scaffold(
      topBar = {
         ProgramTopBar(
            title = stringResource(id = R.string.screen_title_settings)
         )
      },
      modifier = Modifier.fillMaxSize()
   ) { innerPaddings ->
      val appSettings = viewModel.appSettings.value
      val scrollState = rememberScrollState()

      Column(
         modifier = Modifier
            .padding(innerPaddings)
            .fillMaxWidth()
            .verticalScroll(
               state = scrollState
            )
      ) {
         CategoryHeader(
            text = stringResource(id = R.string.settings_header_design),
            modifier = Modifier
               .padding(16.dp)
               .fillMaxWidth()
         )
         Spacer(modifier = Modifier.height(4.dp))


         ThemeSettings(
            appSettings = appSettings,
            viewModel = viewModel
         )
         Spacer(modifier = Modifier.height(4.dp))
         HorizontalDivider()
         Spacer(modifier = Modifier.height(4.dp))

         CategoryHeader(
            text = stringResource(id = R.string.settings_header_handling),
            modifier = Modifier
               .padding(horizontal = 16.dp)
               .fillMaxWidth()
         )
         Spacer(modifier = Modifier.height(4.dp))

         HandlingSettings(appSettings = appSettings, viewModel = viewModel)

         Spacer(modifier = Modifier.height(4.dp))
         HorizontalDivider()
         Spacer(modifier = Modifier.height(4.dp))

         Text(
            text = stringResource(id = R.string.settings_copyright),
            style = MaterialTheme.typography.headlineSmall
         )
         Spacer(modifier = Modifier.height(8.dp))
      }
   }
}

@Composable
fun ThemeSettings(
   appSettings: AppSettings,
   viewModel: SettingsViewModel
) {
   var selectedOption by remember {
      mutableStateOf(appSettings.theme)
   }
   val themeLabels = stringArrayResource(id = R.array.themeLabels)

   Text(
      text = stringResource(id = R.string.setting_theme),
      fontSize = 16.sp,
      style = MaterialTheme.typography.titleMedium,
      modifier = Modifier
         .fillMaxWidth()
         .background(MaterialTheme.colorScheme.background)
         .padding(horizontal = 16.dp)
   )

   Spacer(modifier = Modifier.height(2.dp))

   Column(
      horizontalAlignment = Alignment.Start,
      verticalArrangement = Arrangement.Center,
      modifier = Modifier
         .fillMaxWidth()
         .padding(horizontal = 32.dp)
   ) {
      ThemeSetting.entries.forEachIndexed { index, themeSetting ->
         ThemeItem(
            text = themeLabels[index],
            option = themeSetting,
            selected = selectedOption == themeSetting,
            Modifier
               .padding(4.dp),
            onSelected = {
               selectedOption = it
               viewModel.changeTheme(selectedOption)
            }
         )
         if (index != ThemeSetting.entries.size - 1) {
            Spacer(modifier = Modifier.height(3.dp))
         }
      }
   }
}

@Composable
fun HandlingSettings(
   appSettings: AppSettings,
   viewModel: SettingsViewModel,
   modifier: Modifier = Modifier
) {
   var autoSync by remember {
      mutableStateOf(appSettings.autosync)
   }
   var filesToHold by remember {
      mutableIntStateOf(appSettings.filesToHold)
   }

   Column(
      horizontalAlignment = Alignment.Start,
      verticalArrangement = Arrangement.Center,
      modifier = modifier
   ) {
      AutoSyncItem(
         text = stringResource(id = R.string.setting_auto_sync),
         active = autoSync,
         modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
         onSwitch = {
            autoSync = it
            viewModel.changeAutoSync(it)
         }
      )
      Spacer(modifier = Modifier.height(3.dp))
      FilesToHoldItem(
         text = stringResource(id = R.string.setting_files_to_hold),
         nrFiles = filesToHold,
         modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
         onCountChanged = {
            filesToHold = it
            viewModel.changeFilesToHold(it)
         }
      )
   }
}

