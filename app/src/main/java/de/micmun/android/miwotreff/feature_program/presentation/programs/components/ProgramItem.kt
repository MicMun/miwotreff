/*
 * ProgramItem.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.programs.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.util.toUiDate

/**
 * Row item of one program entry.
 *
 * @author MicMun
 * @version 1.0, 08.08.24
 */
@Composable
fun ProgramItem(
   program: Program,
   modifier: Modifier = Modifier,
   compact: Boolean = true,
   onItemClicked: () -> Unit = {}
) {
   val cardColors = if (program.isCurrentWednesday()) {
      CardDefaults.cardColors().copy(
         containerColor = MaterialTheme.colorScheme.inverseSurface,
         contentColor = MaterialTheme.colorScheme.inverseOnSurface
      )
   } else {
      CardDefaults.cardColors()
   }

   Card(
      colors = cardColors,
      modifier = modifier
         .clickable { onItemClicked() }
   ) {
      Row(
         modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 4.dp),
         horizontalArrangement = Arrangement.Start,
         verticalAlignment = Alignment.Top
      ) {
         Text(
            text = program.date.toUiDate(),
            style = MaterialTheme.typography.bodyLarge,
         )
         Spacer(modifier = Modifier.width(8.dp))
         if (compact) {
            Column(
               modifier = Modifier.weight(1.0f),
               horizontalAlignment = Alignment.Start,
               verticalArrangement = Arrangement.Center
            ) {
               Text(
                  text = program.topic,
                  textAlign = TextAlign.Start,
                  style = MaterialTheme.typography.bodyLarge,
                  fontWeight = FontWeight.Bold,
                  modifier = Modifier.fillMaxWidth()
               )
               program.person?.let { p ->
                  Spacer(modifier = Modifier.height(4.dp))
                  Text(
                     text = p,
                     textAlign = TextAlign.Start,
                     style = MaterialTheme.typography.bodyMedium,
                     modifier = Modifier.fillMaxWidth(),
                     fontStyle = FontStyle.Italic
                  )
               }
            }
         } else {
            Text(
               text = program.topic,
               textAlign = TextAlign.Start,
               style = MaterialTheme.typography.bodyLarge,
               fontWeight = FontWeight.Bold,
               modifier = Modifier.weight(1.0f)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(
               text = program.person ?: "",
               textAlign = TextAlign.End,
               style = MaterialTheme.typography.bodyMedium,
               fontStyle = FontStyle.Italic
            )
         }
      }
   }
}
