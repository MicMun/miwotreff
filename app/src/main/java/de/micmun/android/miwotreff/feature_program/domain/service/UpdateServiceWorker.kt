/*
 * UpdateServiceWorker.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.domain.service

import android.Manifest
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import de.micmun.android.miwotreff.MainActivity
import de.micmun.android.miwotreff.R
import de.micmun.android.miwotreff.common.Constants
import de.micmun.android.miwotreff.feature_program.data.remote.ProgramApi
import de.micmun.android.miwotreff.feature_program.domain.model.Setting
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.feature_program.domain.repository.SettingRepository
import de.micmun.android.miwotreff.util.Resource
import de.micmun.android.miwotreff.util.toLocalDateTime
import de.micmun.android.miwotreff.util.toUiDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.time.LocalDate

/**
 * Update program from api.
 *
 * @author MicMun
 * @version 1.0, 01.09.24
 */
@HiltWorker
class UpdateServiceWorker @AssistedInject constructor(
   private val repository: ProgramRepository,
   private val settingRepository: SettingRepository,
   private val programApi: ProgramApi,
   @Assisted private val context: Context,
   @Assisted workerParameter: WorkerParameters
) : CoroutineWorker(context, workerParameter) {

   companion object {
      const val CHANNEL_ID: String = "miwotreff_channel"
      const val NOTIFICATION_SUCCESS = 101
      const val NOTIFICATION_LOADING = 102
      const val NOTIFICATION_ERROR = 103
   }

   private val notificationManager = context
      .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
   private lateinit var lastServerUpdate: LocalDate

   override suspend fun doWork(): Result {
      val shouldServerUpdate = shouldFetchPrograms()

      if (!shouldServerUpdate)
         return Result.success()
      settingRepository.insertSetting(
         Setting(
            key = Constants.KEY_LAST_UPDATE,
            value = lastServerUpdate.toUiDate()
         )
      )

      return withContext(Dispatchers.IO) {
         var returnvalue = Result.success()

         repository.fetchProgramEntries()
            .collect { result ->
               when (result) {
                  is Resource.Success -> {
                     val fetchResult = result.data!!
                     if (fetchResult.countNew > 0 || fetchResult.countUpdate > 0) {
                        val notification = createNotification(
                           context.getString(R.string.notify_success_title),
                           context.getString(
                              R.string.fetch_result,
                              fetchResult.countNew,
                              fetchResult.countUpdate
                           ),
                           CustomNotificationType.SUCCESS
                        )
                        showNotification(notification, NOTIFICATION_SUCCESS)
                     }
                  }

                  is Resource.Error -> {
                     val notification = createNotification(
                        context.getString(R.string.notify_error_title),
                        context.getString(R.string.notify_error_msg, result.message),
                        CustomNotificationType.ERROR
                     )
                     showNotification(notification, NOTIFICATION_ERROR)
                     returnvalue = Result.failure()
                  }

                  is Resource.Loading -> {
                     if (result.isLoading) {
                        val notification = createNotification(
                           context.getString(R.string.notify_loading_title),
                           text = context.getString(R.string.notify_loading),
                           CustomNotificationType.LOADING
                        )
                        showNotification(notification, NOTIFICATION_LOADING)
                     } else {
                        notificationManager.cancel(NOTIFICATION_LOADING)
                     }
                  }
               }
            }
         returnvalue
      }
   }

   /**
    * Returns <code>true</code>, if last server update newer
    * than local last update.
    *
    * @return Returns <code>true</code>, if last server update newer
    *         than local last update.
    */
   private fun shouldFetchPrograms(): Boolean {
      lastServerUpdate = runBlocking(Dispatchers.IO) {
         programApi.getLastUpdate().pDate.toLocalDateTime()
      }
      val localLocalUpdate: LocalDate = (runBlocking(Dispatchers.IO) {
         settingRepository.getSetting(Constants.KEY_LAST_UPDATE)
      }?.value ?: Constants.DEFAULT_LAST_UPDATE).toLocalDateTime()

      return lastServerUpdate.isAfter(localLocalUpdate)
   }

   private fun createNotification(title: String, text: String, type: CustomNotificationType)
         : Notification {
      val intent = Intent(context, MainActivity::class.java).apply {
         flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
      }
      val pendingIntent = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_IMMUTABLE)

      val silent = type == CustomNotificationType.LOADING

      return NotificationCompat.Builder(context, CHANNEL_ID)
         .setSmallIcon(R.drawable.ic_notification)
         .setContentTitle(title)
         .setContentText(text)
         .setCategory(
            when (type) {
               CustomNotificationType.SUCCESS -> Notification.CATEGORY_STATUS
               CustomNotificationType.LOADING -> Notification.CATEGORY_SERVICE
               else -> Notification.CATEGORY_ERROR
            }
         )
         .setPriority(NotificationCompat.PRIORITY_DEFAULT)
         .setContentIntent(pendingIntent)
         .setAutoCancel(true)
         .setSilent(silent)
         .build()
   }

   private fun showNotification(notification: Notification, notificationId: Int) {
      if (ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.POST_NOTIFICATIONS
         ) != PackageManager.PERMISSION_GRANTED
      ) {
         return
      }
      notificationManager.notify(notificationId, notification)
   }
}

enum class CustomNotificationType {
   SUCCESS, LOADING, ERROR
}