package de.micmun.android.miwotreff.feature_program.data.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.micmun.android.miwotreff.feature_program.domain.model.Setting

/**
 * Entity for settings.
 *
 * @author MicMun
 * @version 1.0, 11.08.24
 */
@Entity(tableName = "settings")
data class SettingEntity(
   @PrimaryKey(autoGenerate = false)
   val key: String,
   val value: String? = null
)

fun SettingEntity.toSetting(): Setting {
   return Setting(key, value)
}
