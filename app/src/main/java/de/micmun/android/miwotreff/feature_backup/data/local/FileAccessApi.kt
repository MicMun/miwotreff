/*
 * FileAccessApi.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_backup.data.local

import de.micmun.android.miwotreff.feature_backup.data.local.model.FileInfoDto

/**
 * Interface for file access api.
 *
 * @author MicMun
 * @version 1.0, 19.09.24
 */
interface FileAccessApi {
   fun readFile(file: FileInfoDto): String
   fun writeFile(fileName: String, content: String)
   fun getFiles(): List<FileInfoDto>
   fun searchLastFile(search: String): FileInfoDto?
   fun deleteFiles(fth: Int): Int
}
