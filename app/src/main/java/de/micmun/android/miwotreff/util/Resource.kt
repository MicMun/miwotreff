/*
 * Resource.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.util

/**
 * Resource for loading in a flow.
 *
 * @author MicMun
 * @version 1.0, 18.08.24
 */
sealed class Resource<T>(val data: T? = null, val message: String? = null) {
   class Success<T>(data: T?) : Resource<T>(data)
   class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)
   class Loading<T>(val isLoading: Boolean = true) : Resource<T>()
}
