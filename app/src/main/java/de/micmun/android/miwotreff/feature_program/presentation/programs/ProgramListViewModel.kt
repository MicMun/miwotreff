/*
 * ProgramListViewModel.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.programs

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.feature_program.domain.repository.ProgramRepository
import de.micmun.android.miwotreff.util.Resource
import de.micmun.android.miwotreff.util.toUiDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for main functions.
 *
 * @author MicMun
 * @version 1.0, 13.07.24
 */
@HiltViewModel
class ProgramListViewModel @Inject constructor(
   private val repository: ProgramRepository
) : ViewModel() {
   var state by mutableStateOf(ProgramListState())

   private var searchJob: Job? = null
   private var dbJob: Job? = null
   private var fetchJob: Job? = null

   init {
      getProgramList()
   }

   fun onEvent(event: ProgramListEvent) {
      when (event) {
         is ProgramListEvent.Refresh -> {
            fetchProgramList()
         }

         is ProgramListEvent.ShowSearchField -> {
            state = state.copy(
               isSearching = event.visible
            )
         }

         is ProgramListEvent.OnSearchQueryChange -> {
            state = state.copy(searchQuery = event.query)
            searchJob?.cancel()
            searchJob = viewModelScope.launch {
               delay(500L)
               getProgramList(event.query)
            }
         }

         is ProgramListEvent.ResetFetchResult -> {
            state = state.copy(
               fetchProgramResult = null
            )
         }
      }
   }

   fun replaceItem(index: Int, program: Program) {
      val programs = mutableListOf<Program>()
      programs.addAll(state.programs)
      programs[index] = program
      state = state.copy(programs = programs.toList())
   }

   private fun getProgramList(query: String = "") {
      dbJob?.cancel()
      dbJob = repository.searchProgramEntries(query)
         .onEach { programs ->
            Log.i("ProgramListViewModel", "programs.size = ${programs.size}")
            state = state.copy(programs = programs)
         }
         .launchIn(viewModelScope)
   }

   private fun fetchProgramList() {
      fetchJob?.cancel()

      fetchJob = viewModelScope.launch(Dispatchers.IO) {
         delay(100L)

         repository.fetchProgramEntries()
            .collect { result ->
               when (result) {
                  is Resource.Success -> {
                     result.data?.let { fetchResult ->
                        state = state.copy(fetchProgramResult = fetchResult)
                     }
                  }

                  is Resource.Error -> {
                     result.message?.let { error ->
                        state = state.copy(error = error)
                     }
                  }

                  is Resource.Loading -> {
                     state = state.copy(isLoading = result.isLoading)
                  }
               }
            }
      }
   }
}
