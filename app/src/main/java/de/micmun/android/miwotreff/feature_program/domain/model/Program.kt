package de.micmun.android.miwotreff.feature_program.domain.model

import androidx.compose.runtime.Immutable
import de.micmun.android.miwotreff.util.toUiDate
import java.time.DayOfWeek
import java.time.LocalDate

/**
 * Data model for program.
 *
 * @author MicMun
 * @version 1.0, 04.08.24
 */
@Immutable
data class Program(
   val id: Int = 0,
   val date: LocalDate,
   val topic: String = "",
   val person: String? = null,
   val isOptionsRevealed: Boolean = false
) {
   fun isCurrentWednesday(): Boolean {
      if (date.dayOfWeek != DayOfWeek.WEDNESDAY)
         return false

      val now = LocalDate.now()
      if (now.equals(date))
         return true

      val weekBefore = date.minusWeeks(1)
      return now.isAfter(weekBefore) && now.isBefore(date)
   }

   override fun toString(): String {
      return "${date.toUiDate()}: $topic ($person)"
   }
}
