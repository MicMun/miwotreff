/*
 * ProgramListEvent.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.presentation.programs

/**
 * Events for user actions.
 *
 * @author MicMun
 * @version 1.0, 08.08.24
 */
sealed class ProgramListEvent {
   object Refresh: ProgramListEvent()
   data class OnSearchQueryChange(val query: String): ProgramListEvent()
   data class ShowSearchField(val visible: Boolean): ProgramListEvent()
   object ResetFetchResult: ProgramListEvent()
}
