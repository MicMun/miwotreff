/*
 * PermissionStateExt.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff

import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState

/**
 * Extension for permission state.
 *
 * @author MicMun
 * @version 1.0, 08.09.24
 */

@OptIn(ExperimentalPermissionsApi::class)
fun PermissionState.isPermanentlyDenied(): Boolean {
   return !shouldShowRationale && !hasPermission
}
