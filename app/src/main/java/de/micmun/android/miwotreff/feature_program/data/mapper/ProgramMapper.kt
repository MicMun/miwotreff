/*
 * ProgramMapper.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.mapper

import de.micmun.android.miwotreff.feature_program.data.local.entities.ProgramEntity
import de.micmun.android.miwotreff.feature_program.data.remote.dto.ProgramDto
import de.micmun.android.miwotreff.feature_program.domain.model.Program
import de.micmun.android.miwotreff.util.toLocalDateTime

/**
 * Mapper for program to dto and entity
 *
 * @author MicMun
 * @version 1.0, 18.08.24
 */
fun ProgramEntity.toProgram(): Program {
   return Program(
      id = id,
      date = date,
      topic = topic,
      person = person
   )
}

fun Program.toProgramEntity(): ProgramEntity {
   return ProgramEntity(
      id = id,
      date = date,
      topic = topic,
      person = person
   )
}

fun ProgramDto.toProgram(): Program {
   return Program(
      date = pDate.toLocalDateTime(),
      topic = pTopic,
      person = pPerson
   )
}
