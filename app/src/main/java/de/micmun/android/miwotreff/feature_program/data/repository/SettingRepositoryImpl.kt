/*
 * SettingRepositoryImpl.kt
 *
 * Copyright 2024 by MicMun
 */
package de.micmun.android.miwotreff.feature_program.data.repository

import de.micmun.android.miwotreff.feature_program.data.local.ProgramDatabase
import de.micmun.android.miwotreff.feature_program.data.local.entities.SettingEntity
import de.micmun.android.miwotreff.feature_program.data.local.entities.toSetting
import de.micmun.android.miwotreff.feature_program.domain.model.Setting
import de.micmun.android.miwotreff.feature_program.domain.repository.SettingRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Repository for settings (implementation).
 *
 * @author MicMun
 * @version 1.0, 07.08.24
 */
class SettingRepositoryImpl @Inject constructor(
   db: ProgramDatabase
) : SettingRepository {
   private val dao = db.settingDao

   override fun getSettings(): Flow<List<Setting>> = flow {
      val settings = dao.getSettings().map { it.toSetting() }
      emit(settings)
   }

   override suspend fun getSetting(key: String): Setting? {
      return dao.findSettingByKey(key)?.toSetting()
   }

   override suspend fun insertSetting(setting: Setting) {
      dao.insertSetting(SettingEntity(setting.key, setting.value))
   }
}
