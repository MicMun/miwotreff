# MiWoTreff

## About

This GitHub repository hosts the code of the Android app _MiWoTreff_. _MiWoTreff_ shows the schedule of the weekly
meetings of the evangelic community of Bogenhausen, Munich.

Dieses GitHub Repository hostet den Code der Android-App _MiWoTreff_. _MiWoTreff_ zeigt das Programm des "Mittwochstreff
der Evangelischen Gemeinschaft Bogenhausen, München".

<a href="https://play.google.com/store/apps/details?id=de.micmun.android.miwotreff">
<img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" height="80"/>
</a>

## Screenshots

<img src="screenshot_list.png" width="270" height="606" alt="Screenshot program list"/>
<img src="screenshot_edit.png" width="270" height="606" alt="Screenshot edit entry"/>

## Compatibility

This app is compatible from sdk version 29 (Android 10.0) upward and needs the following libraries to compile:

* Google Jetpack Compose
* [Simple Storage by Anggrayudi H](https://github.com/anggrayudi/SimpleStorage)
* [Retrofit](https://github.com/square/retrofit)

Die App ist kompatibel ab SDK Version 14 (Android 4.0) aufwärts und braucht folgende Libraries zum Kompilieren:

* Google Jetpack Compose
* [Simple Storage von Anggrayudi H](https://github.com/anggrayudi/SimpleStorage)
* [Retrofit](https://github.com/square/retrofit)

## License

**Copyright 2024 by MicMun**

This program is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see
[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
